﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneControllerGround : MonoBehaviour {

	/* GOAL OF THIS CLASS: */

	public Button button1;						// First switch option button
	public Button button2;						// Second switch option button
	public Text reactionText;					// Text that ellicits a reaction from the player on Trigger
	public PlayerController playerController; 	// Reference to PlayerController script

	public void OnClickButton1() {
		// Change to sky scene
		Application.LoadLevel ("Main");
	}

	public void OnClickButton2() {
		// Deactivate buttons
		reactionText.gameObject.SetActive (false);
		button1.gameObject.SetActive (false);
		button2.gameObject.SetActive (false);
	}

	void Update() {
		playerController.canJump = true;
	}

	// Upon reaching the doorway...
	void OnTriggerEnter2D(Collider2D other) {
		// Set the text for the buttons
		reactionText.text = "Enter?";
		button1.GetComponentInChildren<Text>().text = "Yes";
		button2.GetComponentInChildren<Text>().text = "No";

		// Set the buttons to "Active" so they are visible on screen
		reactionText.gameObject.SetActive (true);
		button1.gameObject.SetActive (true);
		button2.gameObject.SetActive (true);
	}
}
