﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetLevel : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Reload the ground level if the player so happens to fall off the edge. */

	void OnTriggerEnter2D (Collider2D other) {
		Application.LoadLevel (Application.loadedLevel);
	}
}
