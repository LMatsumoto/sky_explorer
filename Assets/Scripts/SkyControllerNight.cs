﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkyControllerNight : MonoBehaviour {

	/* GOAL OF THIS CLASS: 
	 * Properly display text and buttons when the daytime sky is active. */

	public Button button1;					// First switch option button
	public Button button2;					// Second switch option button
	public Text reactionText;				// Text that ellicits a reaction from the player on Trigger
	public PlayerController playerCtrl; 	// Reference to PlayerController script

	void Start() {
		reactionText.text = "";
	}

	void Update() {
		playerCtrl.canJump = true;
	}

	// Upon reaching the moon...
	void OnTriggerEnter2D(Collider2D other) {
		// Set the text color to white for easier reading
		reactionText.color = Color.white;

		// Set the text for the buttons
		reactionText.text = "Pretty chilly out here...";
		button1.GetComponentInChildren<Text>().text = "I'd like to warm up.";
		button2.GetComponentInChildren<Text>().text = "...Is that a castle?";

		// Set the buttons to "Active" so they are visible on screen
		reactionText.gameObject.SetActive (true);
		button1.gameObject.SetActive (true);
		button2.gameObject.SetActive (true);
	}
}
