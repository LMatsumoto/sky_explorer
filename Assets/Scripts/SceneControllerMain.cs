﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneControllerMain : MonoBehaviour {

	/* GOAL OF THIS CLASS: 
	 * Switch the background sky when a button has been pressed. */

	public Text reactionText;			// Reference to reaction text
	public Button reactionBtn;			// Reaction button for castle sky
	public Button button1;				// First button
	public Button button2;				// Second button
	public GameObject player;			// Game object for player
	public GameObject[] skyList;		// List of all possible skies Order: { Day, Night, Castle }

	public void OnClickButton1() {
		Invoke ("SetNewSkyB1", 0.5f);
	}

	public void OnClickButton2() {
		Invoke ("SetNewSkyB2", 0.5f);
	}

	void SetNewSkyB1() {
		if (skyList[0].activeInHierarchy) 				// If the day sky is active...
			SetNewSky(skyList[1]);							// Set up the night sky
		else 										// Otherwise (if the night or castle sky is active)...
			SetNewSky(skyList[0]);								// Set up the day sky
	}

	void SetNewSkyB2() {
		if (skyList[2].activeInHierarchy) 			// If the castle sky is active...
			SetNewSky(skyList[1]); 								// Set up the night sky
		else 										// Otherwise (if the day or night sky is active)...
			SetNewSky(skyList[2]); 							// Set up the castle sky
	}

	void SetNewSky(GameObject newSky) {
		// Deactivate all other skies... and activate the correct one
		foreach (GameObject sky in skyList) { 	
			if (sky.CompareTag(newSky.tag)) 
				sky.SetActive (true);
			else 
				sky.SetActive (false);
		}

		// If the new sky is day or night... Reset the player position to (0, 2)
		if (!newSky.CompareTag ("Castle"))
			player.transform.SetPositionAndRotation (new Vector3 (0, 1), Quaternion.identity);

		// Reset and deactivate the reaction text and buttons
		ResetButtons (); 
	}

	// Reset the reaction text (or button) and other buttons
	void ResetButtons() {
		reactionText.gameObject.SetActive (false);
		reactionBtn.gameObject.SetActive (false);
		button1.gameObject.SetActive (false);
		button2.gameObject.SetActive (false);
	}
}
