﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Camera should follow the player's horizontal movements. */

	public GameObject player; 		// Player to follow with Camera
	public Transform skyFloor;		// Lowest (and leftmost) point camera should pan to
	public Transform skyCeiling;	// Highest (and rightmost) point camera should pan to

	private Vector3 newPosition;	// New camera position

	// Update is called once per frame
	void Update () {
		// Get current camera position
		newPosition = transform.position;

		// Line camera up with the player
		newPosition.x = player.transform.position.x;
		newPosition.y = player.transform.position.y; 

		// Make sure position is between the lowest and highest (and left and rightmost) positions
		newPosition.x = Mathf.Clamp (newPosition.x, skyFloor.position.x, skyCeiling.position.x);
		newPosition.y = Mathf.Clamp (newPosition.y, skyFloor.position.y, skyCeiling.position.y);

		// Set the camera position to this new position (will change as player moves)
		transform.position = newPosition;
	}
}
