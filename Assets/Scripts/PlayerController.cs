﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Make sure the player is moving and jumping at a decent speeed,
	 * while facing the right direction. */

	[HideInInspector] public bool canJump;			// Player not allowed to jump when at castle
	[HideInInspector] public Vector3 origScale;		// Original scale of the player

	public float speed;								// Speed by which to move the player
	public float jumpForce;							// Force that propells the character upward on a jump

	private bool facingRight;						// True when the Sprite is "facing right"
	private bool jump;								// True when Jump key is pressed
	private Rigidbody2D rb2d;						// Get rigidbody2D for the character

	void Start () {
		facingRight = true;
		jump = false;
		rb2d = GetComponent<Rigidbody2D> ();
	}

	void Update() {
		// print ("Can jump: " + canJump);
		/* If the user is holding down the "Jump" key... 
		 * And the setting is such that it is okay for the user to jump
		 * And the user is not currently moving through the air... then jump! */
		if (Input.GetButtonDown ("Jump") && canJump && (rb2d.velocity.y == 0)) {
			jump = true;
		}
	}
	
	void FixedUpdate () {
		// Move the player horizontally...
		float moveHorizontal = Input.GetAxis ("Horizontal");
		Vector2 movement = new Vector2 (moveHorizontal, 0.0f);		
		rb2d.AddForce (movement * speed);

		// Make sure the character is always facing in the direction of its movement
		if (moveHorizontal > 0 && !facingRight)
			Flip ();
		else if (moveHorizontal < 0 && facingRight)
			Flip ();

		// Check if the player is trying to jump... If so, jump!
		if (jump) {
			// print ("attempting to jump");
			rb2d.AddForce(new Vector2 (0.0f, jumpForce));
			jump = false;
		}
	}

	// Flip the player sprite
	void Flip() {
		facingRight = !facingRight;					// Reset boolean
		Vector3 newScale = transform.localScale;	// Get scale coordinate of sprite
		newScale.x *= -1; 							// Multiply the x-axis value to get the opposite counterpart
		transform.localScale = newScale;			// Reset sprite with new x axis scale
	}
}
