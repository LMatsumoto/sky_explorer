﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkyControllerDay : MonoBehaviour {

	/* GOAL OF THIS CLASS: 
	 * Properly display text and buttons when the daytime sky is active. */

	public Button button1;					// First switch option button
	public Button button2;					// Second switch option button
	public Text reactionText;				// Text that ellicits a reaction from the player on Trigger
	public GameObject player;				// Reference to player
	public PlayerController playerCtrl; 	// Reference to PlayerController script

	void Start() {
		reactionText.text = "";
		// Player's position is set here so that upon first entering the Main scene, the player is set in the center
		player.transform.SetPositionAndRotation (new Vector3 (0, 1), Quaternion.identity);
	}

	void Update() {
		playerCtrl.canJump = true;
	}

	// Upon reaching the sun...
	void OnTriggerEnter2D(Collider2D other) {
		// Set the color to black for easier reading
		reactionText.color = Color.black;

		// Set the text for the buttons
		reactionText.text = "Phew! So hot up here!";
		button1.GetComponentInChildren<Text>().text = "Turn down the heating.";
		button2.GetComponentInChildren<Text>().text = "...Is that a castle?";

		// Set the buttons to "Active" so they are visible on screen
		reactionText.gameObject.SetActive (true);
		button1.gameObject.SetActive (true);
		button2.gameObject.SetActive (true);
	}
}
