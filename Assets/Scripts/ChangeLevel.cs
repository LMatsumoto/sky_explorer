﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeLevel : MonoBehaviour {

	/* GOAL OF THIS CLASS:
	 * Upon falling from one of the clouds... set up the Ground level. */

	void OnTriggerEnter2D(Collider2D other) {
		Application.LoadLevel ("Ground");
	}
}
