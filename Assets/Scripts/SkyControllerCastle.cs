﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkyControllerCastle : MonoBehaviour {

	/* GOAL OF THIS CLASS: 
	 * Properly display text and buttons when the daytime sky is active. */

	public Button reactionBtn;				// Button with reaction text... Using a button to see black text on white
	public Button button1;					// First switch option button
	public Button button2;					// Second switch option button
	public PlayerController playerCtrl;		// Reference to player game object

	void Update() {
		playerCtrl.canJump = false; // Make sure the player cannot jump when at the castle
		StartCoroutine(CastleUpdate());
	}

	IEnumerator CastleUpdate() {
		yield return new WaitForSeconds (5.0f);
		SetButtons ();
	}

	void SetButtons() {
		// Set the text for the buttons
		reactionBtn.GetComponentInChildren<Text>().text = "The sky is so dark... Everything is pitch black.";
		button1.GetComponentInChildren<Text>().text = "Turn on the lights.";
		button2.GetComponentInChildren<Text>().text = "Where are the stars?";

		// Set the buttons to "Active" so they are visible on screen
		reactionBtn.gameObject.SetActive (true);
		button1.gameObject.SetActive (true);
		button2.gameObject.SetActive (true);
	}
}
